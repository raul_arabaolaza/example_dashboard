/**
 * Created with JetBrains WebStorm.
 * User: raul
 * Date: 16/07/13
 * Time: 21:04
 */

'use strict'

define(['angular'],function(angular){
    return angular.module('DashboardAppUIModule',['ui.bootstrap']);
});
